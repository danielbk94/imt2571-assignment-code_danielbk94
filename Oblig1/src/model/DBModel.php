<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
			try
			{
				$this->db = new PDO('mysql:host=mysql.ansatt.ntnu.no;dbname=daniebk_library_A1;charset=utf8mb4', 'daniebk', 'dbkSQL!');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e)
			{
				echo $e->getMessage();
				//die();
			}
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
 	{
		 $booklist = array();
		try
		{
		 foreach($this->db->query('SELECT * FROM book') as $row) {
			 $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id'] );
		 }

		 return $booklist;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}


	 }

	 /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		if ($id != ' ' && $id != NULL) {
			try {
				$stmt = $this->db->prepare("SELECT * FROM book WHERE id=:id");
				$stmt->execute(array(':id' => $id));

				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				$book = new Book($row['title'], $row['author'], $row['description'], $row['id'] );
				return $book;
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}
		else {
			echo "ID is not valid";
			die();
		}
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if ($book->title != ' ' && $book->title != NULL && $book->author != ' ' && $book->author != NULL){
			try {
				$stmt = $this->db->prepare ("INSERT INTO book(title, author, description)
				VALUES(:title, :author, :description)");
				$stmt->execute(array(
					':title' => $book->title,
					':author' => $book->author,
					':description' => $book->description
					));
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}
		else {
			echo "Either title or author is empty or NULL";
			die();
		}

    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if ($id != ' ' && $id != NULL) {
			if ($book->title != ' ' && $book->title != NULL && $book->author != ' ' && $book->author != NULL){
				try {
					$stmt = $this->db->prepare ("UPDATE book
										SET title=:title, author=:author, description=:description
										WHERE id=:id");
					$stmt->execute(array(
						':id' => $book->id,
						':title' => $book->title,
						':author' => $book->author,
						':description' => $book->description
						));
				}
				catch(PDOException $e)
				{
					echo $e->getMessage();
				}
			}
			else {
				echo "Either title or author is empty or NULL";
				die();
			}
		}
		else {
			echo "ID is not valid";
			die();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		if ($id != ' ' && $id != NULL) {
			try {
				$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
				$stmt->execute(array(
					':id' => $id
					));

			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}
		else {
			echo "ID is not valid";
			die();
		}
    }

}

?>